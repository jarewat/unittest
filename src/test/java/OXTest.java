/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxgame_oop.Game;
import com.mycompany.oxgame_oop.Player;
import com.mycompany.oxgame_oop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jarew
 */
public class OXTest {
    
    public OXTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
     public void testColWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkCol(1));
    }
     @Test
     public void testCol2Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkCol(2));
    }
     @Test
     public void testCol3Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkCol(3));
    }
     @Test
     public void testRowWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkRow(1));
    }
     public void testRow2Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkRow(2));
    }
     public void testRow3Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkRow(3));
    }
     @Test
     public void testX1Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkX1());
    }
     @Test
     public void testX2Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkX2());
    }
     @Test
     public void testCheckWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin(3,3));
    }
     @Test
     public void testCheckNotWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        assertEquals(false, table.checkWin(2,2));
    }
}
